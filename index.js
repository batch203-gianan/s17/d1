console.log ("Hello World");
// [SECTION] Functions
// Functions in javascript are lines/blocks of codes that tell out device/application to perform a certain task when called/invoked.
// They are also used to prevent repeating lines/blocks of codes that perform the same task/function.

// Function declarations
	// function statement defined a function with specified parameters.
	/*
		Syntax:

			function functionName (){
				code block (statement)
			}

			function keyword - use to define a javascript function.

			functionName - the function name is used so we are albe to call/invoke our declared function.

			function code block ({}) - the statements which compromise the body of the function. THisis where the code to executed
	*/

	function printName(){
		console.log ("My Name is John");
	}

// [SECTION] Function Invocation
	// It is common use the term "call a function" instead of "invoke a function".
	// Let's invoke/call the function that was declared.

	printName();

	declaredFunction(); // - results in a n error, much like variables, we cannot invoke a function that we have not define yet.

	// declared functions can be hoisted. As long as the function has been defined.
		// Hoisting is JavaScript's behavior for cetain variables(var) and functions to run or use them before the declaration

	function declaredFunction(){
		console.log("Hello World");
	}

	declaredFunction();

// [SECTION] FUnction Declaration vs Expression
	// Function Declaration
		// A function can be created through function declaration by using the function keyword and adding a function name

	// Function Expression
		// A function can be also stored in a variable. This is called as function expression

		// A function expression is an anonymous function assigned to the variable variableFunction.
			// Anonymous function - a function without a name.

		// variableFunction(); error - function expressions, being stored in a lot or const variable. cannot be hoisted.

		let variableFunction = function() {
			console.log ("Hello Again!");
		}

		variableFunction();

		// We can also create a function expression of a named function
		let funcExpression = function funcName (){
			console.log ("Hello from the other side.");
		}
		// funcExpression = declaredFunction ();

		funcExpression();
		// funcName(); result to an not defined function.

		declaredFunction();

		// Reassigning a declared function and expression.
		declaredFunction = function(){
			console.log("updated declaredFunction");
		}

		declaredFunction();

		functionExpression = function (){
			console.log("updated funcExpression.");
		}

		functionExpression();

		const constantFunc = function (){
			console.log("Initialized with const");
		}
		constantFunc();

		// However, we cannot re-assign a function expression initialized with const.
		// constantFunc = function(){
		// 	console.log("New Value");
		// }

		// constantFunc();

// [SECTION] Function Scoping
/*
	Scope is the accessibility/visibility of a variables in the code.

	Javascript Variables has 3 types of scopes:
	1. local/block scope
	2. global scope
	3. function scope

*/
	//Variables declared inside a {} block can only be access locally
	// Local and Block scope only works with let and const
	{
		let localVar = "Armando Perez";
		// var localVar = "Armando Perez";
		console.log (localVar);

	}

	let globalVar = "Mr. Worldwide";

	console.log (globalVar);
	// console.log (localVar); //result in error
	
	//Function Scopes
		// Javascript has function scopes: Each function createsa new scopes
		// Variables defined inside a function are not accessible/visible outside the function.
		// Variables declared with var, let and const are quite similar when declared inside a function

	function showNames(){
		//Function scoped variables:
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}

	showNames();
	// Error - This are function scoped variable and cannot be accessed outisde a function they were declared in.
	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);


// Nested Functions
	// You can create another function inside a function.
	// This is called a nested function.

	function myNewFunction(){
		let name = "Jane";

		function nestedFunction(){
			let nestedName = "John";
			console.log(name);
		}

		// console.log(nestedName); // result to not defined error.
		nestedFunction();
	}

	myNewFunction();
	//nestedFunction(); //result to not defined error

	// Function and Global Scope variables
	let globalName = "Alexandro";

	function myNewFunction2(){
		let nameInside = "Renz";
		console.log (globalName);
	}

	myNewFunction2();
	//console.log(nameInside);

// [SECTION] Using alert()
	// alert () allows us to show a small window at the top of our browser page to show information to our users.
	// It allows us to show a short dialog or insturctions to our users. THe page will wait until the user dismisses the dialog.

	//alert ("Hello WOrld"); // THis will run immediately run when the page loads

	function showSampleAlert(){
		alert ("Hello, User!");
	}

	// showSampleAlert();

	console.log ("I will only log in the console when the alert it dismissed.");

	// Notes on the use of alert (); :
		// Show only an alert(); for short dialogs/messages to the user.
		// Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

// [SECTION] Using prompt();
	// prompt() allow us to show small window at the top of the browser to gather user input.
	// The input form the prompt() will be returned as a "String" once the user dismisses the window.

	/*
		Syntax:

			let variable = prompt ("<dialogInString>");
	*/

	// let samplePrompt = prompt ("Enter your Full Name");

	//console.log (typeof samplePrompt);

	//console.log ("Hello, " + samplePrompt);

	//let sampleNullPrompt = prompt ("Don't enter anything");
	// prompt() returns an empty string (" ") when there is no input, or null if the user cancels the prompt.
	//console.log (sampleNullPrompt);

	//Let's create a function scoped variable that will store the returned data from our prompt();.

	function printWelcomeMessage(){
		let firstName = prompt ("Enter your first name: ");
		let lastName = prompt("Enter your last name: ");

		console.log ("Hello, " + firstName + " " + lastName + "!");
		console.log ("Welcome to my page!");
	}

	printWelcomeMessage();

// [SECTION] Function Naming Convention
	// Function name should be definitive of the task it will perform. It usually contains a verb

	function getCourses(){
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log (courses);
	}

	getCourses();

	//Avoid generic names to avoid confusion within our code.

	//Avoid pointless and inappropriate function names, example: foo, bar
		// THis are "metasyntactic variable" which are set of words indentified as a placeholder in computer programming
